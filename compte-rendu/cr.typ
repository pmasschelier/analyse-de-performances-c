#import "sourcerer.typ": code
#import "template.typ": *

#show image: box

#let code = code.with(stroke: none, fill: none, line-offset: 2em)

#show: template.with(
    ue: [A0 -- Architecture des processeurs],
    devoir: [TP1 -- Analyse de performances],
    auteurs: (
        auteur("Ianis", "Giraud"),
        auteur("Mustapha", "Handang"),
        auteur("Mohammed", "Khorsi"),
        auteur("Paul-Marie", "Maschelier")
    ),
    date: datetime(year: 2023, month: 12, day: 1),
    toc-depth: 1,
    intro: [
= Introduction
L'objectif de ce TP est d'une part de se familiariser avec les outils
de mesure des performances et d'autre part d'observer l'effet des
options de compilation et du cache sur les temps d'exécution.
        
Pour ce faire, nous allons compiler et exécuter plusieurs fonctions en
variant les paramètres de taille des données et les options de compilation.

== Méthode
La fonction d'affichage a été modifiée pour calculer et écrire un
temps d'exécution moyen, après avoir exclu les valeurs aberrantes
(@list-print_res). Un script shell est alors utilisé pour compiler et
exécuter le programme avec plusieurs options de compilation
(@list-shell) et enregistrer les résultats dans un fichier
texte. Enfin, un script python permet de générer des histogrammes afin
de comparer les résultats selon les différents paramètres.

Les mesures ont été réalisées sur un processeur avec les tailles de cache suivantes :
#code[```bash
$ lspcu
...
Caches (somme de toutes) :                  
  L1d :                                     128 KiB (4 instances)
  L1i :                                     128 KiB (4 instances)
  L2 :                                      1 MiB (4 instances)
  L3 :                                      6 MiB (1 instance)
```]

        
#figure(code[```bash
#!/bin/sh

for opt in "-O0" "-O1" "-O2" "-O3"; do
    echo -e "\nCompiler option: $opt\n"
             
    for type in "char" "short" "int" "uint64_t" "float" "double"; do
        echo -e "\nType: $type\n"
             
        for n in 100 250 500 750 1000; do
            echo -e "\nN = $n\n"
            gcc $opt -DN=$n -DTYPE=$type tp1.c -o tp1 -lm
            ./tp1
done; done; done```],
        caption: [Script shell permettant de mesurer les performances
                  des fonctions pour chaque option]
) <list-shell>
#figure(code[```C
void print_res(char *funcname, double nb_iter) {
    double normalize = 1.0 / nb_iter;
    double norm_res[M];
    for (int i = 0; i < M; i++) {
        norm_res[i] = resultats[i] * normalize;
    }
    printf("Function: %s\n", funcname);
    double sum = 0.0;
    double sum2 = 0.0;
    for (int i = 0; i < M; i++) {
        double tmp = norm_res[i];
        sum += tmp;
        sum2 += tmp * tmp;
    }
    const double mean = sum / M;
    const double var = (sum2 / M) - mean * mean;
    unsigned int nb_coherent_results = 0;
    
    sum = 0.0;
    for (int i = 0; i < M; i++) {
        double tmp = norm_res[i];
        printf("%g, ", tmp);
        double ecart = absd(tmp - mean) / 2;
        if(ecart * ecart < var) {
            sum += tmp;
            nb_coherent_results++;
        }
    }
    printf("\n");
    const double coherent_mean = sum / nb_coherent_results;
    printf("Operation count: %d\n", (int)nb_iter);
    printf("Iterations kept: %d/%d\n", nb_coherent_results, M);
    printf("Average cycle count: %g\n\n", coherent_mean);
}
         ```],
        caption: [Fonction d'affichage modifiée]
) <list-print_res>
    ])

#set heading(numbering: "1.1.")
= Optimisations du compilateur

#figure([
		#image("figures/Q2-0.svg")
	],
	caption: [Effet de l'optimisation sur le temps d'exécution]
) <figopt>

La @figopt représente le nombre de cycle par itération de quelques fonctions en
fonction du niveau d'optimisation. On observe que, pour toutes les
fonctions et tous les types de donnée, augmenter le niveau
d'optimisation de `-O0` à `O2` permet d'accélérer drastiquement l'exécution
du programme. En revanche, passer de `O2` à `O3` ne permet pas d'augmenter
sensiblement les performances.

= Étude de diverses fonctions

== Mise à zéro d’un vecteur

#figure([
		#image("figures/Q3-1.svg")
	],
	caption: [Vitesse d'execution de la fonction ZERO selon le type de variable]
) <figzero>

La @figzero représente le nombre de cycle par itération de la fonction ZERO selon le type de variable des vecteurs.
On constate que le nombre de cycle croît en fonction de la taille en octet du type utilisé.
Regardons l'assembleur généré pour la fonction zero:
#figure(code[```yasm
...
rdtscp
mov    %edx,%ebp
mov    %eax,%esi
shl    $0x20,%rbp
mov    %esi,%esi
mov    $0xf4240,%edx
mov    $0x4f9360,%edi
  401c09:	48 09 f5             	or     %rsi,%rbp
xor    %esi,%esi
call   401050 <memset@plt>
rdtscp
mov    %edx,%esi
mov    %eax,%edi
cpuid
...
```],
        caption: [Assembleur généré pour la fonction zero compilée avec -O3 -DN=1000 -DTYPE=char]
) <list-asm>

On constate que la boucle for a été remplacée par un `memset` ce qui permet de faire un transfert de mémoire indépendant de la taille de la variable, par bloc. À taille de bloc fixé on peut donc transférer plus de données pour des variables de taille plus petite.
Lorsque `memset` met à zero 8 `char` il ne met dans le même temps à zero qu'un seul `uint64_t`.

Par ailleurs on constate que plus le vecteur est petit plus son nombre moyen de cycle par itération est faible.
Cela s'explique par la taille du cache. Plus le vecteur est grand plus le nombre d'échecs de cache sera élevé.
C'est ce qui explique le pic particulièrement élevé pour les vecteurs de taille 1000 avec des types de 8 octets.

== Copie de matrices

#figure([
		#image("figures/Q4-1.svg")
	],
	caption: [Vitesse d'execution des fonctions COPY_ij et COPY_ji selon la taille du vecteur]
) <figcopy>

La @figcopy représente le nombre de cycle par itération des fonctions COPY_ij et COPY_ji selon la taille du vecteur.
On constate que la fonction COPY_ij est systématiquement plus rapide que COPY_ji et l'écart s'accroit avec la taille des vecteurs.
En effet la fonction COPY_ij copie la matrice ligne par ligne tandis que la COPY_ji la copie colonne par colonne.
Or dans le langage C pour une matrice `mat[ligne][colonne]` tous les éléments d'une `mat[ligne]` seront contigus en mémoire.
Ainsi parcourir la matrice ligne par ligne est beaucoup plus efficace vis-à-vis du cache que de la parcourir colonne par colonne.

== Addition de matrices

#figure([
		#image("figures/Q5-1.svg")
	],
	caption: [Vitesse d'execution des fonctions ADD_ij et ADD_ji selon la taille du vecteur]
) <figadd>

La @figadd permet d'arriver aux mêmes conclusions que la @figcopy : plus un vecteur est grand plus le nombre de cache miss est élévé, et il est bien plus efficace de parcourir une matrice colonne par colonne que ligne par ligne.

== Produit scalaire de deux vecteurs

#figure([
		#image("figures/Q6-1.svg")
	],
	caption: [Vitesse d'execution de la fonction `ps` selon la taille du vecteur]
) <figps>

La @figps montre que le nombre de cycle par itération ne varie pas selon la taille du vecteur.
En effet dans un produit scalaire chaque vecteur n'est parcouru qu'une fois et est composé de blocs contigus de mémoire.
Les seuls échecs de cache qui ont lieu sont des échecs obligatoire puisque les données ne sont pas réaccédées après.
De plus comme les vecteurs sont parcourus linéairement ont peut supposer que les prefetch adéquats ont lieu. Auquel cas aucun échec de cache n'a lieu.

= Produit de matrices

== Produit de matrices ijk de flottants 32 bits

#figure([
		#image("figures/Q7-1.svg")
	],
	caption: [Vitesse d'execution de la fonction `mm_ijk` selon la taille du vecteur]
) <figmmijk>

La @figmmijk montre qu'à partir d'une taille de matrice N = 1000. le nombre de cycle par itération commence à croître significativement. Ceci est dû aux défauts de cache qui augmentent significativement alors que le cache ne peut plus contenir l'intégralité des matrices qui sont parcourues.
Dans ce cas l'on aurait une taille de cache S = 8Mo
Soit un ordre de grandeur de 10Mo.
Ceci correspond au résultat renvoyé par la commande `lspcu` pour laquelle les caches L1 L2 L3 additionnée pour un core avaient une taille totale de 7.1Mo.

== Produit de matrices ikj

#figure([
		#image("figures/Q8-1.svg")
	],
	caption: [Vitesse d'execution de la fonction `mm_ikj` selon la taille du vecteur]
) <figmmbikj>

La fonction `mm_ikj` est plus rapide que la fonction `mm_ijk` pour toutes les tailles de matrice $N >= 20$.
Faisons l'hypothèse de ligne de cache de 64 octets.
Alors pour $N <= 16$ une ligne de la matrice ($4×10 = 40 "octets"$) tient dans une ligne de cache.

Pour une taille $N < 16$, lorsque l'on parcourt la matrice colonne par colonne, une partie de la colonne suivante va donc être chargée on a donc un prefetch partiel même en cas de parcours ligne par ligne.

De manière générale la fonction `mm_ikj` améliore la multiplication de matrices en utilisant la localité des accès mémoires grâce au calcul de tous les éléments d'une ligne de la matrice résultat simultanément.

Cela évite à la fois beaucoup d'échecs obligatoire car le parcours se fait en adéquation avec les lignes de cache mais aussi les échecs de capacité. En effet l'on n'aperçoit plus l'augmentation du nombre de cycles par itération après $N = 1000$, car cet algorithme n'a pas besoin d'avoir l'intégralité des matrices chargée en cache à chaque itération. Les trois matrices sont parcourues ligne par ligne et donc une itération n'a besoin que d'avoir qu'une ligne de chaque matrice chargée en cache.

== Produit de matrices ijk par blocs

#figure([
		#image("figures/Q9-1.svg")
	],
	caption: [Vitesse d'execution de la fonction `mm_B_ijk` selon la taille du vecteur]
) <figmmikj>

L'algorithme `mm_B_ijk` permet lui aussi d'aobtenir une amélioration du produit de matrice en exploitant la localité des données. De nouveau les échecs de capacité disparaissent car seuls certains blocs de taille fixe 16*N, N*16 et N*N sont nécessaire à chaque multiplication matricielle et ceux ci rentrent dans le cache.

Cependant contrairement à la version `mm_ikj` les multiplications de blocs de matrices sont faits dans l'ordre ijk et occasionnent donc beaucoup de défauts de caches obligatoires.

== Produit de matrices avec transposition

#figure([
		#image("figures/Q10-1.svg")
	],
	caption: [Vitesse d'execution de la fonction `mm_trans_ijk` selon la taille du vecteur]
) <figmmtransijk>

L'algorithme `mm_trans_ijk` de même que les deux précédents permet d'obtenir une amélioration du produit de matrice en exploitant la localité des données. Cette fois-ci en transposant la matrice de droite pour transformer les accès colonne par colonne en accès ligne par ligne.

#figure([
		#image("figures/Q10-2.svg")
	],
	caption: [Vitesse d'execution de la fonction `mm_trans_ijk` selon la taille du vecteur]
) <figmmtransijk2>

La @figmmtransijk2 montre que la multiplcation de matrice est de 2 à 3 fois plus rapide en nombre de cycle par itération pour les entiers que pour les flottants comme on peut s'y attendre. On obtient un nombre de cycles par itération ordonné : int < uint64_t < float < double

= Questions additionnelles

== Comparaison du produit scalaire et de la multiplication de matrices en flottant

#figure([
		#image("figures/Q11-1.svg")
	],
	caption: [Vitesse d'execution des fonctions `ps`, `mm_ijk`, `mm_ikj` selon la taille du vecteur]
) <figps2>

== Multiplication de matrices en entier et en flottant

#figure([
		#image("figures/Q13-1.svg")
	],
	caption: [Vitesse d'execution de la fonctions `mm_ikj` selon le type de variable du vecteur]
) <figps3>

== Produit scalaire en entier et en flottant

#figure([
		#image("figures/Q12-1.svg")
	],
	caption: [Vitesse d'execution des fonctions `ps`, `mm_ijk`, `mm_ikj` selon la taille du vecteur]
) <figps3>

On remarque que le produit scalaire est aussi rapide que l'algorithme mm_ikj pour les int.


