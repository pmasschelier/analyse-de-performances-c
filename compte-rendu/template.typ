
#let auteur(prenom, nom) = (prenom: prenom, nom: nom)

#let template(
    ue: none,     // L'intitulé de l'UE
    devoir: none, // L'intitulé du devoir/TP/TD
    auteurs: (),  // La liste des auteurs, fournis avec auteur(prenom, nom)
    toc-depth: 1, // La profondeur de la table des matières. 0: pas de sommaire
    intro: none,  // L'introduction (non numérotée)
    date: auto,   // La date. none: pas de date, auto: aujourd'hui, datetime
    body
) = {
    let ht-first = state("page-first-section", [])
    let ht-last = state("page-last-section", [])

    let mulline-join(width: 100%, sep: [ ], ..content) = {
        if type(width) == ratio {
            layout(size => {
                mulline-join(width: size.width*width, sep: sep, ..content.pos())
            })
        } else {
            content.pos().fold(none, (retval,elem) => {
                style(styles => {
                    if (measure([#retval#sep#elem], styles).width <= width) {
                        [#retval#sep#elem]
                    } else {
                        [#retval\ #elem]
                    }
                })
            })
        }
    }

    // Mets à jour ht-first et ht-last pour l'en-tête, mais ne retourne rien
    let update-section(loc) = {
        // find first heading of level 1 on current page
        let first-heading = query(
            heading.where(level: 1), loc
        ).find(h => h.location().page() == loc.page())
        
        // find last heading of level 1 on current page
        let last-heading = query(
            heading.where(level: 1), loc
        ).rev()
         .find(h => h.location().page() == loc.page())
                    
        // test if the find function returned none (i.e. no headings on this page)
        //repr((first-heading, last-heading, loc.page()))
        if not first-heading == none {
            ht-first.update([
                #if counter(heading).at(first-heading.location()).at(0) != 0 {
                    numbering("1.", ..counter(heading).at(first-heading.location()))
                }
                #first-heading.body
            ])
            ht-last.update([
                #if counter(heading).at(last-heading.location()).at(0) != 0 {
                    numbering("1.", ..counter(heading).at(last-heading.location()))
                }
                #last-heading.body
            ])
        }
    }

    // Mets à jour ht-first et ht-last pour l'en-tête, et retourne la section courante
    let get-section(loc) = {
        // find first heading of level 1 on current page
        let first-heading = query(
            heading.where(level: 1), loc
        ).find(h => h.location().page() == loc.page())
        
        // find last heading of level 1 on current page
        let last-heading = query(
            heading.where(level: 1), loc
        ).rev()
         .find(h => h.location().page() == loc.page())
                    
        // test if the find function returned none (i.e. no headings on this page)
        //repr((first-heading, last-heading, loc.page()))
        if not first-heading == none {
            ht-first.update([
                #if counter(heading).at(first-heading.location()).at(0) != 0 {
                    numbering("1.", ..counter(heading).at(first-heading.location()))
                }
                #first-heading.body
            ])
            ht-last.update([
                #if counter(heading).at(last-heading.location()).at(0) != 0 {
                    numbering("1.", ..counter(heading).at(last-heading.location()))
                }
                #last-heading.body
            ])
            ht-first.display()
        } else {
            ht-last.display()
        }
    }

    // Localisation fr : séparateur de décimales
    show math.equation : it => {
        show regex("\d\.\d"): it => {show ".": {","+h(0pt)}
            it}
        it
    }

    set par(leading: 0.55em, first-line-indent: 1.8em, justify: true)

    // Formatte le titre
    let title = if (ue != none) [#ue\ #devoir] else [#devoir]

    // Formatte la date
    date = if date == auto {
        datetime.today()
    } else {date}
    date = if (date != none) {
        date.display("[day]/[month]/[year]")
    } else {date}


    set page(
        margin: (top: 2.5cm, bottom: 1.5cm, x: 3cm),

        // Gauche : section courante
        header: locate(loc => {
                let pagenum = counter("page").at(loc).at(0)

                if pagenum != 0 {
                    grid(columns: (50%, 50%),
                        align(left + top,
                            text(style: "italic",
                                get-section(loc)
                            )
                        ),
                        // align(right + top, {
                        //     set par(justify: false)
                        //     set text(hyphenate: false)
                        //     mulline-join(sep: h(2em), ..auteurs.map(it =>  [#it.prenom~#smallcaps(it.nom)]))
                        //     parbreak()
                        //     date
                        // })
                    )
                } else {update-section(loc)}
        }),

        // Centre : numéro de page
        footer: locate(loc => {
            counter("page").step()
            align(center, [#counter("page").display()/#numbering("1", ..counter("page").final(loc))])
        })
    )

    v(-1.5cm)
    align(center,block(above: 0cm, below: 1em, 
        text(2em, weight: "bold", title))
    )
    align(center, auteurs.map(it => box[#it.prenom #smallcaps(it.nom)]).join([, ], last: [ & ]))

    // Affiche la table des matières
    if (toc-depth != 0) {
        heading(numbering: none, level: 1, outlined: false)[Table des matières]
        outline(title: none, depth: toc-depth)
    }

    // Affiche l'introduction
    if (intro != none) {
        //set heading(numbering: none)
        intro
        pagebreak()
    }

    // Numérotation
    set heading(numbering: (..it) => {
        if (it.pos().len() == 1) [
            #it.pos().at(0)#h(1em)
        ] else [
        ]
    })
    show heading.where(level: 1): it => {
        block(above: 1.5em)[
            #counter(heading).display()
            #it.body
        ]
    }
    
    body
}
