#!/bin/python3

import sys
import matplotlib.pyplot as plt

def convertir_en_entier_ou_caractere(chaine):
    try:
        entier = int(chaine)
        return entier
    except ValueError:
        return chaine

def choose(values, question):
	for i in range(len(values)):
		print(str(i) + ") " + str(values[i]))
	return int(input(question))


# Vérifiez si le nombre d'arguments est correct
if len(sys.argv) != 3:
    print("Usage: python script.py <nom_du_fichier.log> <nom_du_fichier.png>")
    sys.exit(1)

# Récupérez le nom du fichier à partir des arguments
nom_du_fichier = sys.argv[1]

results = []
types = set()
counts = set()
options = set()
functions = set()

# Ouvrez le fichier en mode lecture
with open(nom_du_fichier, 'r') as fichier:
	# Parcourez chaque ligne du fichier
	option = "-O0"
	type = "char"
	count = 100
	for ligne in fichier:
		# Vérifiez si la ligne contient "Compiler option: "
		option_token = "Compiler option: "
		type_token = "Type: "
		count_token = "N = "
		result_token = "Average cycle count: "
		function_token = "Function: "
        
		if ligne.startswith(option_token):
			option = ligne[len(option_token):].strip()
			options.add(option)
		if ligne.startswith(type_token):
			type = ligne[len(type_token):].strip()
			types.add(type)
		if ligne.startswith(count_token):
			count = int(ligne[len(count_token):].strip())
			counts.add(count)
		if ligne.startswith(function_token):
			function = ligne[len(function_token):].strip()
			functions.add(function)
		if ligne.startswith(result_token):
			result = float(ligne[len(result_token):].strip())
			results.append({
				"option": option, 
				"type": type, 
				"count": count, 
				"function": function,
				"result": result
			})

types = list(types)
counts = sorted(list(counts))
options = sorted(list(options))
functions = list(functions)
		
# for r in results:
# 	print(r)

categories = ["option", "type", "count", "function"]
labels = ["Niveau d'optimisation", "Types de variable", "Taille des vecteurs", "Fonction"]
colors = ["red", "blue", "purple", "green"]
print("0) Optimisation du compilateur")
print("1) Type de variable")
print("2) Taille des vecteurs")
print("3) Fonction")
choice = int(input("Selon quel paramètre voulez-vous comparer ? "))

xvalues = []
yvalues = []
legends = []

cont = True

while cont:
	results_copy = results.copy()
	if choice != 0:
		option = choose(options, "Choississez l'option du compilateur: ")
		results_copy = filter(lambda e: e["option"] == options[option], results_copy)
	if choice != 1:
		type = choose(types, "Choisisser le type des variables: ")
		results_copy = filter(lambda e: e["type"] == types[type], results_copy)
	if choice != 2:
		count = choose(counts, "Choississez la taille des vecteurs: ")
		results_copy = filter(lambda e: e["count"] == counts[count], results_copy)
	if choice != 3:
		function = choose(functions, "Choisisser la fonction: ")
		results_copy = filter(lambda e: e["function"] == functions[function], results_copy)

	if choice == 0:
		title = functions[function] + ": N = " + str(counts[count]) + ", type = " + types[type]
		# title = "Comparaison de la fonction " + functions[function] + " sur des vecteurs de " + str(counts[count]) + " " + types[type]
	if choice == 1:
		title = functions[function] + " ( " + options[option] + " ): N = " + str(counts[count])
		# title = "Comparaison de la fonction " + functions[function] + " compilée avec " + options[option] + " sur des vecteurs de " + str(counts[count]) + " éléments"
	if choice == 2:
		title = functions[function] +  " ( " + options[option] + " ): type = " + types[type]
		# title = "Comparaison de la fonction " + functions[function] + " compilée avec " + options[option] + " sur des vecteurs de " + types[type]
	if choice == 3:
		title = "N = " + str(counts[count]) + ", type = " + types[type] + " ( " + options[option] +" )"
		# title == "Comparaisons des différentes fonctions" + "compilée avec " + options[option] + " sur des vecteurs de " + str(counts[count]) + " " + types[type]

	results_copy = list(results_copy)
	# print(results_copy)

	xvalue = [e[categories[choice]] for e in results_copy]
	yvalue = [e["result"] for e in results_copy]

	xvalues.append(xvalue)
	yvalues.append(yvalue)
	legends.append(title)

	print(xvalue)
	print(yvalue)

	cont = ("Y" == input("Voulez vous ajoutez une autre ligne au graphe ? (Y/N): "))


# Créer le graphique en barres
plt.figure(figsize=(10,6))
for i in range(len(yvalues)):
	plt.plot(xvalues[i], yvalues[i], label = legends[i])

# Ajouter des labels et un titre
plt.xlabel(labels[choice])
plt.ylabel('Nombre moyen de cycle par itération')

# plt.title(title)
plt.legend()

# plt.autoscale()
#plt.axis(ymin = 0)

# Afficher le graphique
plt.savefig(sys.argv[2], dpi = 200)
plt.show()