#!/bin/sh

for opt in "-O0" "-O1" "-O2" "-O3"; do
    echo -e "\nCompiler option: $opt\n"
             
    for type in "char" "short" "int" "uint64_t" "float" "double"; do
        echo -e "\nType: $type\n"
             
        for n in 100 250 500 750 1000; do
            echo -e "\nN = $n\n"
            gcc $opt -DN=$n -DTYPE=$type tp1.c -o tp1 -lm
            ./tp1
done; done; done
